/* eslint-disable react/destructuring-assignment */
/* eslint-disable prefer-template */
import React from 'react';

const Fish = (props) => (
  <p>
    {'><' + ('=').repeat(props.size) + '°>'}
  </p>
);

export default Fish;
