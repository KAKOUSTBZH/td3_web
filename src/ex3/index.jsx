/* eslint-disable react/button-has-type */
import React from 'react';
import Fish from './Fish';

const fishSize = 2;

// eslint-disable-next-line import/prefer-default-export
export const ExerciseThreePage = () => (
  <main>
    <Fish size={fishSize} />
  </main>
);
