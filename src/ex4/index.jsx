/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable implicit-arrow-linebreak */
import React, { useEffect, useState } from 'react';
import './ex4.css';
import Superheroes from './Superheroes';

// eslint-disable-next-line import/prefer-default-export
export const ExerciseFourView = () => {
  // eslint-disable-next-line no-unused-vars
  const [superheroes, setSuperheroes] = useState([]);

  useEffect(() => {
    fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json')
      .then((res) => res.json())
      .then((heroes) => setSuperheroes(heroes));
  });
  const notStrongHeroes = superheroes.filter((superhero) => superhero.powerstats.strength < 7);

  function Contenu(heroesSelect) {
    const result = heroesSelect.map((heroCurrent) =>
      <Superheroes
        image={heroCurrent.images.sm}
        name={heroCurrent.name}
        alignment={heroCurrent.biography.alignment}
        strength={heroCurrent.powerstats.strength}
      />);
    return result;
  }
  return (
    <div>
      <h2>Pourquoi les héros les moins forts ne sont pas méchants ? </h2>
      {
        superheroes.length > 0 && (
          <div className="tousProfils">{Contenu(notStrongHeroes)}</div>
        )
      }
    </div>
  );
};
