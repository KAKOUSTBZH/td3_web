/* eslint-disable prefer-template */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable import/newline-after-import */
/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
const Superheroes = (props) => (
  <div className="profil">
    <img className="imagestyle" src={props.image} />
    <h3> {props.name}
    </h3>
    <h4> {props.alignment}
    </h4>
    <h4> {'force : ' + props.strength + '/100'}
    </h4>
  </div>
);

export default Superheroes;
